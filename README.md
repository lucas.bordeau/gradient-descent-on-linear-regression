# Gradient descent self-teaching notebook

This notebook teaches gradient descent on a simple linear regression problem.

<img src="resources/target_scatter_plot.png" width="250" />

It is intended for educational purposes, and is heavily inspired by the excellent [fast.ai](https://course.fast.ai/) course, taught by Jeremy Howard.

You will find a bit of maths and matrix operations, as I tried my best to connect the dots between linear algebra and linear regression.

## Demo

You can see an example of what we do in this notebook, a gradient descent animation with different learning rates : 

| Learning rate | Animation |
| :-------------: | :-------------: |
| **0.01** | <img src="resources/learning_rate_0_01.gif"/><br>Too slow, the gradient descent will take forever  |
| **1**  | <img src="resources/learning_rate_1.gif"/><br>Too fast, the gradient descent cannot make it go to the bottom, it jumps to far away  |
| **0.4**  | <img src="resources/learning_rate_0_4.gif"/><br>Good, we can go to the optimal solution fast |

## Install

### Cloud based
You may want to directly download this repo on a cloud jupyter notebook service like [Paperspace](https://www.paperspace.com/) or a virtual Linux server instance with Jupyter already installed.

### Local
Depending on your OS, please see the official documentation to install these prerequisites : 
- [Python 3+](https://www.python.org/downloads/)
- [Anaconda](https://www.anaconda.com/distribution/) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html) (recommended lighter version)
- [Jupyter](https://jupyter.org/install)

### Setup
Install the libraries :

`conda install -c pytorch -c fastai fastai`

Launch the Jupyter notebook server with : 

`jupyter notebook`

### Run
Navigate to this notebook with the Jupyter web interface, open it and play with it !

See [jupyter notebook help](https://jupyter-notebook.readthedocs.io/en/stable/) if you are not familiar with Jupyter

## Support

The Fastai library works best on Linux, it is untested on Mac and Windows, please report an issue if you can't make it run.

You can also see the [official documentation](https://course.fast.ai/index.html#using-a-gpu) directly on the Fastai course introduction page.

You may also find help in the Fastai [installation issue](https://github.com/fastai/fastai/blob/master/README.md#installation-issues).

## Contributing

Merge Requests are welcome.

## License
[MIT](https://choosealicense.com/licenses/mit/)
